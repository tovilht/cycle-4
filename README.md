# Cycle Four

# Extreme Gradient Booster

1. In light of the opinion of KTL, CNN was on hold while the XGBoost was used

# XGBoost with Raw data

1. performed well on validation with training data
2. does not perform well on unseen, different DNA data
3. Problem: the XGBoost trees were learning the noise from the training data

# In search of a optimal segment length

1. segments length tried: 1, 2, 3, 4, 5, 6, 8, 10, 12k, since 12 million is cannot be divided by 7000, 9000, 11000
2. the accuracy plateau around 8000 - 10000 data points, around a 97.5% validation accuracy

# Baseline Correction

1. with raw data, the classifer will learn the noise
2. baseline correction was applied with:

```py
import numpy as np
from scipy import sparse
from scipy.sparse.linalg import spsolve

def baseline_als_optimized(y, lam, p, niter=10):
    L = len(y)
    D = sparse.diags([1,-2,1],[0,-1,-2], shape=(L,L-2))
    D = lam * D.dot(D.transpose()) # Precompute this term since it does not depend on `w`
    w = np.ones(L)
    W = sparse.spdiags(w, 0, L, L)
    for i in range(niter):
        W.setdiag(w) # Do not create a new matrix, just update diagonal values
        Z = W + D
        z = spsolve(Z, w*y)
        w = p * (y > z) + (1-p) * (y < z)
    return z
```

3. the FFT data were better than the raw FFT data

# Training on Baseline corrected FFT data

1. accuracy attained at around 97% as well, not big difference

# Baseline Correction vs Raw Data in unseen, different DNA

1. the one with raw data has an accuracy at around 78%
2. that with baseline correction is 99.3%
3. baseline correction is need for XGBoost in order to eliinate the learning on noise
